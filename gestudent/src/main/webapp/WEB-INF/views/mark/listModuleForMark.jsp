<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter Module</title>
	
</head>

<body>
	<strong>Choisissez le Module voulu</strong>
	<br>
	<br>
	<c:forEach var="module" items="${listModule}">
	Module <c:out value="${module.idModule}"/> :
	
	<br>
		Nom : <c:out value="${module.name}"/>
	<br>
		Heurs : <c:out value="${module.numberHours}"/>
	<br>
		Description : <c:out value="${module.description}"/>
	<br>
    <a href="markModule?idModule=${ module.idModule }&idMark=${mark.idMark}" class="text-blue-400 hover:text-blue-600 underline">Valider</a>
	<br>
	<br>
	<br>
	</c:forEach>
	
	
</body>

</html>
