<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter Module</title>
	
</head>

<body>

	<c:forEach var="mark" items="${listMark}">
	Note <c:out value="${mark.idMark}"/> :
	
	<br>
		Valeur : <c:out value="${mark.value}"/>
	<br>
		Module : <c:out value="${mark.module.name}"/>
	<br>
    <a href="editMark?id=${ mark.idMark }" class="text-blue-400 hover:text-blue-600 underline">Editer</a>
    <a href="deleteMark?id=${ mark.idMark }" class="text-blue-400 hover:text-blue-600 underline pl-6">Supprimer</a>
	<br>
	<br>
	<br>
	</c:forEach>
	
	<a href="addMark" class="text-blue-400 hover:text-blue-600 underline"><strong>Ajouter une note</strong></a>
    
	
	
	
</body>

</html>
