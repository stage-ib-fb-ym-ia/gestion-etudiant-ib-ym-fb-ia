<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
	<meta charset="utf-8">
	<title>Connexion TEMPORAIRE</title>
	
</head>

<body>

<h3>CONNEXION ADMIN</h3>
	<form action="singinAdmin" method="post" class="login">

	<div class="container">

		<div class="login-box animated fadeInUp">
			<div class="box-header">
			</div>
			<label for="username">Login</label>
			<br/>
			<input type="text"
			placeholder="Login" name="login"> 
			<br/>
			<label for="password">mot de passe</label>
			<br/>
			<input type="password"
			placeholder="Mot de passe" name="pwd">
			<br/>
			<button type="submit">Connexion</button>
			<br/>
			<a href="addAdmin"><p class="small">Pas de compte ADMIN? Inscrivez vous</p></a>

		</div>
	</div>
</form>
<br>
<h3>CONNEXION USER</h3>
<form action="singinUser" method="post" class="login">

	<div class="container">

		<div class="login-box animated fadeInUp">
			<div class="box-header">
			</div>
			<label for="username">Login</label>
			<br/>
			<input type="text"
			placeholder="Login" name="login"> 
			<br/>
			<label for="password">mot de passe</label>
			<br/>
			<input type="password"
			placeholder="Mot de passe" name="pwd">
			<br/>
			<button type="submit">Connexion</button>
			<br/>
			<a href="addUser"><p class="small">Pas de compte USER? Inscrivez vous</p></a>

		</div>
	</div>
</form>
<br>
<h3>CONNEXION TEACHER</h3>
<form action="singinTeacher" method="post" class="login">

	<div class="container">

		<div class="login-box animated fadeInUp">
			<div class="box-header">
			</div>
			<label for="login">Login</label>
			<br/>
			<input type="text"
			placeholder="Login" name="login"> 
			<br/>
			<label for="pwd">mot de passe</label>
			<br/>
			<input type="password"
			placeholder="Mot de passe" name="pwd">
			<br/>
			<button type="submit">Connexion</button>
			<br/>
			<a href="addTeacher"><p class="small">Pas de compte TEACHER? Inscrivez vous</p></a>

		</div>
	</div>
</form>

</body>

</html>
