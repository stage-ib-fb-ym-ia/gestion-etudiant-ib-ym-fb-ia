<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ajouter un formateur</title>
</head>
<body>

	<form action="addTeacher" method="post">
		
		<label for="gender">Monsieur</label>
		<input type="radio" name="gender" value="Monsieur" required>
  		
  		<label for="gender">Madame</label>
  		<input type="radio" name="gender" value="Madame" required><br/>	
				
		<label for="userTeacher">Nom</label><br/>
		<input type="text" placeholder="Nom" name="name"><br/>
				
		<label for="userTeacher">Pr�nom</label><br/>
		<input type="text" placeholder="Pr�nom" name="surname"><br/>
				
		<label for="userTeacher">Date de naissance</label><br/>
		<input type="date" placeholder="Date de naissance" name="date"><br/>
		
		<label for="userTeacher">T�l�phone</label><br/>
		<input type="tel" placeholder="T�l�phone" name="phone"><br/>
		
		<label for="userTeacher">Email</label><br/>
		<input type="email" placeholder="Email" name="email"><br/>
  		
				
				
		<label for="addressTeacher">Adresse</label><br/>
		<input type="text" placeholder="N� et Rue" name="street"><br/>
				
		<label for="addressTeacher">Ville</label><br/>
		<input type="text" placeholder="Ville" name="city"><br/>
				
		<label for="addressTeacher">Code postal</label><br/>
		<input type="text" placeholder="Code postal" name="zipCode"><br/>
				
		<label for="addressTeacher">Pays</label><br/>
		<input type="text" placeholder="Pays" name="country"><br/>
		
		
		
		<label for="accountTeacher">Identifiant</label><br/>
		<input type="text" placeholder="Identifiant" name="login"><br/>
			
		<label for="accountTeacher">Mot de passe</label><br/>
		<input type="password" placeholder="Mot de passe" name="pwd"><br/>

		<button type="submit">VALIDER</button>

	</form>

</body>
</html>