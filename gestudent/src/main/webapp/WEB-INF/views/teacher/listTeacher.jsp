<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestion Etudiant User Dashboard</title>
    <meta name="author" content="name">
    <meta name="description" content="description here">
    <meta name="keywords" content="keywords,here">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js" integrity="sha256-xKeoJ50pzbUGkpQxDYHD7o7hxe0LaOGeguUidbq6vis=" crossorigin="anonymous"></script>

</head>

<body class="bg-gray-800 font-sans leading-normal tracking-normal mt-12">

    <!--Nav-->
    <nav class="bg-white pt-2 md:pt-1 pb-1 px-1 mt-0 h-auto fixed w-full z-20 top-0">

        <div class="flex flex-wrap items-center pr-4
">
            <div class="flex flex-shrink md:w-1/3 justify-center md:justify-start text-white">
                <a href="#">

                    <img class=" md:object-top"
             src="${pageContext.request.contextPath}/resources/images/ekole21.png" width="75" height="85"
             alt="Grapefruit slice atop a pile of other slices">
                </a>
            </div>

            <div class="ml-96  md:w-1/3 md:justify-end object-right-top" >
                <ul class="list-reset flex justify-between flex-1 md:flex-none items-center">
                    <li class="flex-1 md:flex-none md:mr-3">

                    </li>
                    <li class="flex-1 md:flex-none md:mr-3">

                    </li>
                    <li class="flex-1 md:flex-none md:mr-3">
                        <div class="relative inline-block">

                            <button onclick="toggleDD('myDropdown')" class="drop-button text-black focus:outline-none"> <span class="pr-2"><i class="em em-robot_face"></i></span>  <svg class="h-3 fill-current inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg></button>
                            <div id="myDropdown" class="dropdownlist absolute bg-gray-800 text-white right-0 mt-3 p-3 overflow-auto z-30 invisible">
                                <input type="text" class="drop-search p-2 text-gray-600" placeholder="Search.." id="myInput" onkeyup="filterDD('myDropdown','myInput')">
                                <a href="#" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fa fa-user fa-fw"></i>Profil</a>
                                <a href="#" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fa fa-cog fa-fw"></i>Param�tre</a>
                                <div class="border border-gray-800"></div>
                                <a href="singoutUser" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fas fa-sign-out-alt fa-fw"></i>Deconnexion</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </nav>



    <div class="flex flex-col md:flex-row">
      <br>
      <br>
        <div class="bg-gray-800 shadow-xl h-16 fixed bottom-0 mt-12 md:relative md:h-screen z-10 w-full md:w-48">

            <div class="md:mt-12 md:w-48 md:fixed md:left-0 md:top-0 content-center md:content-start text-left justify-between">
                <ul class="list-reset flex flex-row md:flex-col py-0 md:py-3 px-1 md:px-2 text-center md:text-left">
                    	<br>
                        <br>
                        <br>
                        <br>
                     <li class="mr-3 flex-1">
                        <a href="listTeacher?idAccount=${ account.idAccount }" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2  border-gray-800 hover:border-blue-500 rounded">
                            <i class="text-blue-600"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block"><p>>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Formateurs</strong></p></span>
                          </a>
                        </li>
                        <br>
                        
                    <li class="mr-3 flex-1">
                        <a href="listClassroom?idAccount=${ account.idAccount }" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2  border-gray-800 hover:border-blue-500 rounded">
                            <i class="text-blue-600"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block"><p>>&nbsp;&nbsp;&nbsp;&nbsp;Classes</p></span>
                          </a>
                        </li>

  					<br>

                    <li class="mr-3 flex-1">
                        <a href="listStudent?idAccount=${ account.idAccount }" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800 hover:border-blue-500 rounded">
                            <i class=" text-blue-600"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">>&nbsp;&nbsp;&nbsp;&nbsp;Etudiants</span>
                          </a>
                      </li>

  					<br>

                    <li class="mr-3 flex-1">
                        <a href="listFormation?idAccount=${ account.idAccount }" class="block py-1 md:py-3 pl-0 md:pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800  hover:border-blue-500 rounded">
                            <i class=""></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">>&nbsp;&nbsp;&nbsp;&nbsp;Formations</span>
                          </a>
                      </li>

  					<br>

                    <li class="mr-3 flex-1">
                        <a href="listModule?idAccount=${ account.idAccount }" class="block py-1 md:py-3 pl-0 md:pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800  hover:border-blue-500 rounded0">

                            <i class=""></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">>&nbsp;&nbsp;&nbsp;&nbsp;Modules</span>
                          </a>
                      </li>
                </ul>
            </div>

        </div>

        <div class="main-content flex-1 pt-5 bg-gray-100 mt-12 md:mt-2 pb-24 md:pb-5" >

          <div class="flex flex-1 md:w-1/3 justify-center md:justify-start text-white px-2 ml-96 pt-6 ">
              <span class="relative w-full">
                  <input type="search" placeholder="Entrez votre recherche..." class="w-full bg-gray-900 text-white transition border border-transparent focus:outline-none  focus:border-gray-400 rounded py-3 px-2 pl-10 appearance-none leading-normal">
                  <div class="absolute search-icon" style="top: 1rem; left: .9rem;">
                      <svg class="fill-current pointer-events-none text-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                          <path d="M12.9 14.32a8 8 0 1 1 1.41-1.41l5.35 5.33-1.42 1.42-5.33-5.34zM8 14A6 6 0 1 0 8 2a6 6 0 0 0 0 12z"></path>
                      </svg>
                  </div>
              </span>
          </div>	
                
               <!--Tableaux de personne-->
        <div class="md:px-32 py-8 ">
        <div class="shadow overflow-hidden rounded border-b border-gray-200">
        <table class="min-w-full bg-white">
          <thead class="bg-gray-800 text-white">
            <tr>
            
              <th class="w-1/4 text-center uppercase font-semibold text-sm"></th>
              <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">Nom</th>
              <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">Pr�nom</th>
              <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">T�l�phone</th>
              <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">E-mail</th>
              <th class="w-1/5 text-center py-3 px-4 uppercase font-semibold text-sm">Identifiant</th>
              <th class="text-center py-3 px-4 uppercase font-semibold text-sm">Voir Plus</th>
              <th class="text-center py-3 px-4 uppercase font-semibold text-sm">Modifier</th>
              <th class="text-center py-3 px-4 uppercase font-semibold text-sm">Supprimer</th>

            </tr>
          </thead>
        <tbody class="text-gray-700">
        
         <c:forEach var="teacher" items="${listTeacher}">
         
          <tr>
           
           	<td class="w-1/3 text-center py-3 px-4">${teacher.gender}</td>
            <td class="w-1/3 text-center py-3 px-4">${teacher.name}</td>
            <td class="w-1/3 text-center py-3 px-4">${teacher.surname}</td>
            <td class="w-1/3 text-center py-3 px-4">${teacher.phone}</td>
            <td class="w-1/3 text-center py-3 px-4">${teacher.email}</td>
            <td class="w-1/3 text-center py-3 px-4">${teacher.account.login}</td>
            <td class="w-1/3 text-center py-3 px-4"><a href="updateTeacher?idTeacher=${teacher.idTeacher}"><img class=" md:object-top" src="${pageContext.request.contextPath}/resources/images/modifier.png" width="35" height="35" alt="Grapefruit slice atop a pile of other slices"></a></td>
            <td class="w-1/3 text-center py-3 px-4"><a href="updateTeacher?idTeacher=${teacher.idTeacher}"><img class=" md:object-top" src="${pageContext.request.contextPath}/resources/images/modifier.png" width="35" height="35" alt="Grapefruit slice atop a pile of other slices"></a></td>
			<td class="w-1/3 text-center py-3 px-4"><a href="deleteTeacher?idTeacher=${teacher.idTeacher}"><img class=" md:object-top" src="${pageContext.request.contextPath}/resources/images/modifier.png" width="35" height="35" alt="Grapefruit slice atop a pile of other slices"></a></td>

          </tr>
          
		</c:forEach>
		
        </tbody>
        </table>
        </div>
        </div>
        <div class="flex flex-col items-center my-12">
        <div class="flex text-gray-700">
        <div class="h-12 w-12 mr-1 flex justify-center items-center rounded-full bg-gray-200 cursor-pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left w-6 h-6">
              <polyline points="15 18 9 12 15 6"></polyline>
          </svg>
        </div>
        <div class="flex h-12 font-medium rounded-full bg-gray-200">
          <div class="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">1</div>
          <div class="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full bg-teal-600 text-white ">2</div>
          <div class="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">3</div>
          <div class="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">...</div>
          <div class="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">13</div>
          <div class="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">14</div>
          <div class="w-12 md:flex justify-center items-center hidden  cursor-pointer leading-5 transition duration-150 ease-in  rounded-full  ">15</div>
          <div class="w-12 h-12 md:hidden flex justify-center items-center cursor-pointer leading-5 transition duration-150 ease-in rounded-full bg-teal-600 text-white">2</div>
        </div>
        <div class="h-12 w-12 ml-1 flex justify-center items-center rounded-full bg-gray-200 cursor-pointer">
          <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right w-6 h-6">
              <polyline points="9 18 15 12 9 6"></polyline>
          </svg>
        </div>
        </div>
        </div>
 
            </div>

        </div>

		

        <script>
        /*Toggle dropdown list*/
        function toggleDD(myDropMenu) {
            document.getElementById(myDropMenu).classList.toggle("invisible");
        }
        /*Filter dropdown options*/
        function filterDD(myDropMenu, myDropMenuSearch) {
            var input, filter, ul, li, a, i;
            input = document.getElementById(myDropMenuSearch);
            filter = input.value.toUpperCase();
            div = document.getElementById(myDropMenu);
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        }
        // Close the dropdown menu if the user clicks outside of it
        window.onclick = function(event) {
            if (!event.target.matches('.drop-button') && !event.target.matches('.drop-search')) {
                var dropdowns = document.getElementsByClassName("dropdownlist");
                for (var i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (!openDropdown.classList.contains('invisible')) {
                        openDropdown.classList.add('invisible');
                    }
                }
            }
        }
        </script>

</body>

</html>
