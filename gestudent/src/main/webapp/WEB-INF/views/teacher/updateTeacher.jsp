<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modifier un formateur</title>
</head>
<body>

	<form action="updateTeacher" method="post">
			
		<input type="text" value="${teacher.idTeacher}" name="idTeacher" readonly="readonly" hidden="true">	
				
		<label for="gender">Monsieur</label>
		<input type="radio" name="gender" value="Monsieur" required>
  		
  		<label for="gender">Madame</label>
  		<input type="radio" name="gender" value="Madame" required><br/>
				
		<label for="userTeacher">Nom</label><br/>
		<input type="text" value="${teacher.name}" name="name"><br/>
				
		<label for="userTeacher">Pr�nom</label><br/>
		<input type="text" value="${teacher.surname}" name="surname"><br/>
				
		<label for="userTeacher">Date de naissance</label><br/>
		<input type="date" value="${teacher.dateBirth}" name="date"><br/>
		
		<label for="userTeacher">T�l�phone</label><br/>
		<input type="tel" value="${teacher.phone}" name="phone"><br/>
		
		<label for="userTeacher">Email</label><br/>
		<input type="email" value="${teacher.email}" name="email"><br/>
  		
		<input type="text" value="${teacher.address.idAddress}" name="idAddress" readonly="readonly" hidden="true">	
				
		<label for="addressTeacher">Adresse</label><br/>
		<input type="text" value="${teacher.address.street}" name="street"><br/>
				
		<label for="addressTeacher">Ville</label><br/>
		<input type="text" value="${teacher.address.city}" name="city"><br/>
				
		<label for="addressTeacher">Code postal</label><br/>
		<input type="text" value="${teacher.address.zipCode}" name="zipCode"><br/>
				
		<label for="addressTeacher">Pays</label><br/>
		<input type="text" value="${teacher.address.country}" name="country"><br/>
		
		
		
		<input type="text" value="${teacher.account.idAccount}" name="idAccount" readonly="readonly" hidden="true">
		
		<label for="accountTeacher">Identifiant</label><br/>
		<input type="text" value="${teacher.account.login}" name="login"><br/>
			
		<label for="accountTeacher">Mot de passe</label><br/>
		<input type="password" value="${teacher.account.password}" name="pwd"><br/>

		<button type="submit">VALIDER</button>

	</form>

</body>
</html>