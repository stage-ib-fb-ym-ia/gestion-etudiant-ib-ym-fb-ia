<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestion Etudiant User Dashboard</title>
    <meta name="author" content="name">
    <meta name="description" content="description here">
    <meta name="keywords" content="keywords,here">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
    <link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    <link href="https://afeld.github.io/emoji-css/emoji.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js" integrity="sha256-xKeoJ50pzbUGkpQxDYHD7o7hxe0LaOGeguUidbq6vis=" crossorigin="anonymous"></script>

</head>


<body class="bg-gray-800 font-sans leading-normal tracking-normal mt-12">

    <!--Nav-->
    <nav class="bg-white pt-2 md:pt-1 pb-1 px-1 mt-0 h-auto fixed w-full z-20 top-0">

        <div class="flex flex-wrap items-center pr-4
">
            <div class="flex flex-shrink md:w-1/3 justify-center md:justify-start text-white">
                <a href="#">

                    <img class=" md:object-top"
             src="${pageContext.request.contextPath}/resources/images/ekole21.png" width="75" height="85"
             alt="Grapefruit slice atop a pile of other slices">
                </a>
            </div>

            <div class="ml-96  md:w-1/3 md:justify-end object-right-top" >
                <ul class="list-reset flex justify-between flex-1 md:flex-none items-center">
                    <li class="flex-1 md:flex-none md:mr-3">

                    </li>
                    <li class="flex-1 md:flex-none md:mr-3">

                    </li>
                    <li class="flex-1 md:flex-none md:mr-3">
                   <strong>${ user.name } ${ user.surname }</strong> 
                        <div class="relative inline-block">

                            <button onclick="toggleDD('myDropdown')" class="drop-button text-black focus:outline-none"> <span class="pr-2"><i class="em em-robot_face"></i></span>  <svg class="h-3 fill-current inline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg></button>
                            <div id="myDropdown" class="dropdownlist absolute bg-gray-800 text-white right-0 mt-3 p-3 overflow-auto z-30 invisible">
                                <input type="text" class="drop-search p-2 text-gray-600" placeholder="Search.." id="myInput" onkeyup="filterDD('myDropdown','myInput')">
                                <a href="#" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fa fa-user fa-fw"></i>Profil</a>
                                <a href="#" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fa fa-cog fa-fw"></i>Param�tre</a>
                                <div class="border border-gray-800"></div>
                                <a href="singoutUser" class="p-2 hover:bg-gray-800 text-white text-sm no-underline hover:no-underline block"><i class="fas fa-sign-out-alt fa-fw"></i>Deconnexion</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </nav>



    <div class="flex flex-col md:flex-row">
      <br>
      <br>
        <div class="bg-gray-800 shadow-xl h-16 fixed bottom-0 mt-12 md:relative md:h-screen z-10 w-full md:w-48">

            <div class="md:mt-12 md:w-48 md:fixed md:left-0 md:top-0 content-center md:content-start text-left justify-between">
                <ul class="list-reset flex flex-row md:flex-col py-0 md:py-3 px-1 md:px-2 text-center md:text-left">
                    	<br>
                        <br>
                        <br>
                        <br>
                     <li class="mr-3 flex-1">
                        <a href="listTeacher?idAccount=${ user.idUser }" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2  border-gray-800 hover:border-blue-500 rounded">
                            <i class="text-blue-600"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block"><p>>&nbsp;&nbsp;&nbsp;&nbsp;Formateurs</p></span>
                          </a>
                        </li>
                        <br>
                        
                    <li class="mr-3 flex-1">
                        <a href="listClassroom?idAccount=${ user.idUser }" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2  border-gray-800 hover:border-blue-500 rounded">
                            <i class="text-blue-600"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block"><p>>&nbsp;&nbsp;&nbsp;&nbsp;Classes</p></span>
                          </a>
                        </li>

  					<br>

                    <li class="mr-3 flex-1">
                        <a href="listStudent?idAccount=${ user.idUser }" class="block py-1 md:py-3 pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800 hover:border-blue-500 rounded">
                            <i class=" text-blue-600"></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">>&nbsp;&nbsp;&nbsp;&nbsp;Etudiants</span>
                          </a>
                      </li>

  					<br>

                    <li class="mr-3 flex-1">
                        <a href="listFormation?idAccount=${ user.idUser }" class="block py-1 md:py-3 pl-0 md:pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800  hover:border-blue-500 rounded">
                            <i class=""></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">>&nbsp;&nbsp;&nbsp;&nbsp;Formations</span>
                          </a>
                      </li>

  					<br>

                    <li class="mr-3 flex-1">
                        <a href="listModuleUser?idAccount=${ user.idUser }" class="block py-1 md:py-3 pl-0 md:pl-1 align-middle text-white no-underline hover:text-white border-b-2 border-gray-800  hover:border-blue-500 rounded0">

                            <i class=""></i><span class="pb-1 md:pb-0 text-xs md:text-base text-gray-600 md:text-gray-400 block md:inline-block">>&nbsp;&nbsp;&nbsp;&nbsp;Modules</span>
                          </a>
                      </li>
                </ul>
            </div>

        </div>

        <div class="main-content flex-1 pt-5 bg-gray-100 mt-12 md:mt-2 pb-24 md:pb-5" >

           <!-- formulaire -->
           <div class="flex justify-center items-center h-screen w-full  ">
    <div class="w-1/2 bg-white rounded shadow-2xl p-8 m-4">
        <h1 class="block w-full text-center text-gray-800 text-2xl font-bold mb-6">AJOUTER UN MODULE</h1>
        <form action="addModuleUser" method="post">
        
                <input type="text" name="idUser" value="${ user.idUser }" hidden="true">
                
            <div class="flex flex-col mb-4">
                <label class="mb-2 font-bold text-lg text-gray-900" for="libelle">Libell�</label>
                <input class="border py-2 px-3 text-grey-800" type="text" name="libelle" id="first_name">
            </div>
            <div class="flex flex-col mb-4">
                <label class="mb-2 font-bold text-lg text-gray-900" for="hour">Nombre d'heures</label>
                <input class="border py-2 px-3 text-grey-800" type="text" name="hour" id="email">
            </div>
            <div class="flex flex-col mb-4">
                <label class="mb-2 font-bold text-lg text-gray-900" for="description">Description</label>
                <input class="border py-2 px-3 text-grey-800" type="text" name="description" id="last_name">
            </div>
            
            <button class="block bg-gray-800 hover:bg-blue-400 text-white font-bold uppercase text-lg mx-auto p-4 rounded" type="submit">CREER</button>
        </form>
    </div>
</div>





    <script>
        /*Toggle dropdown list*/
        function toggleDD(myDropMenu) {
            document.getElementById(myDropMenu).classList.toggle("invisible");
        }
        /*Filter dropdown options*/
        function filterDD(myDropMenu, myDropMenuSearch) {
            var input, filter, ul, li, a, i;
            input = document.getElementById(myDropMenuSearch);
            filter = input.value.toUpperCase();
            div = document.getElementById(myDropMenu);
            a = div.getElementsByTagName("a");
            for (i = 0; i < a.length; i++) {
                if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    a[i].style.display = "";
                } else {
                    a[i].style.display = "none";
                }
            }
        }
        // Close the dropdown menu if the user clicks outside of it
        window.onclick = function(event) {
            if (!event.target.matches('.drop-button') && !event.target.matches('.drop-search')) {
                var dropdowns = document.getElementsByClassName("dropdownlist");
                for (var i = 0; i < dropdowns.length; i++) {
                    var openDropdown = dropdowns[i];
                    if (!openDropdown.classList.contains('invisible')) {
                        openDropdown.classList.add('invisible');
                    }
                }
            }
        }
    </script>


</body>

</html>
    