<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter Module</title>
	
</head>

<body>

	<c:forEach var="formation" items="${listFormation}">
	Formation <c:out value="${formation.idFormation}"/> :
	
	<br>
		Nom : <c:out value="${formation.name}"/>
	<br>
		Manager : <c:out value="${formation.manager}"/>
	<br>
    <a href="editFormation?id=${ formation.idFormation }" class="text-blue-400 hover:text-blue-600 underline">Editer</a>
    <a href="deleteFormation?id=${ formation.idFormation }" class="text-blue-400 hover:text-blue-600 underline pl-6">Supprimer</a>
	<br>
	<br>
	<br>
	</c:forEach>
	
	
</body>

</html>
