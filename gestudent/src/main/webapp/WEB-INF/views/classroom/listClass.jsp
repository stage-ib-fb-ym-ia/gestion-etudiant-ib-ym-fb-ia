<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>List Classroom</title>
</head>
<body>
	<c:forEach var="classroom" items="${listClass}">
	Module <c:out value="${classroom.idClass}"/> :
	
	<br>
		Nom : <c:out value="${classroom.name}"/>
	<br>
		numero : <c:out value="${classroom.number}"/>
	<br>
    <a href="editClassroom?id=${classroom.idClass}" class="text-blue-400 hover:text-blue-600 underline">Editer</a>
    <a href="deleteClassroom?id=${classroom.idClass}" class="text-blue-400 hover:text-blue-600 underline pl-6">Supprimer</a>
	<br>
	<br>
	<br>
	</c:forEach>
</body>
</html>