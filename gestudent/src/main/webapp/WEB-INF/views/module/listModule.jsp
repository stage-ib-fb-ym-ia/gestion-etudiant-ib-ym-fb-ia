<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter Module</title>
	
</head>

<body>

	<c:forEach var="module" items="${listModule}">
	Module <c:out value="${module.idModule}"/> :
	
	<br>
		Nom : <c:out value="${module.name}"/>
	<br>
		Heurs : <c:out value="${module.numberHours}"/>
	<br>
		Description : <c:out value="${module.description}"/>
	<br>
    <a href="editModule?id=${ module.idModule }" class="text-blue-400 hover:text-blue-600 underline">Editer</a>
    <a href="deleteModule?id=${ module.idModule }" class="text-blue-400 hover:text-blue-600 underline pl-6">Supprimer</a>
	<br>
	<br>
	<br>
	</c:forEach>
	
	
</body>

</html>
