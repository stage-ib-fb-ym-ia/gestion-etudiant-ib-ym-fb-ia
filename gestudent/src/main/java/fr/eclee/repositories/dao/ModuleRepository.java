package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Module;

public interface ModuleRepository extends JpaRepository<Module, Integer>  {

}
