package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Classroom;



public interface ClassRepository extends JpaRepository<Classroom, Integer>{

}
