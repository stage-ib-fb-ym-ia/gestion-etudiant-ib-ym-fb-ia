package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Integer> {

}
