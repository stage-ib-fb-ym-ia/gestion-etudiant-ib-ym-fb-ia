package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.AdminAppli;

public interface AdminRepository extends JpaRepository<AdminAppli, Integer> {

}
