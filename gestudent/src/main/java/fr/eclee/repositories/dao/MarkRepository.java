package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Mark;


public interface MarkRepository extends JpaRepository<Mark, Integer>  {

}
