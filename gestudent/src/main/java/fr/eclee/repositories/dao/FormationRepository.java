package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Formation;


public interface FormationRepository extends JpaRepository<Formation, Integer>  {

}
