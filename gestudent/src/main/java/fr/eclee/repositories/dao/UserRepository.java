package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.UserAppli;

public interface UserRepository extends JpaRepository<UserAppli, Integer> {

}
