package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {

}
