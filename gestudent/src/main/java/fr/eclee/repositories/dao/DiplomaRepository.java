package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Diploma;

public interface DiplomaRepository  extends JpaRepository<Diploma, Integer>{

}
