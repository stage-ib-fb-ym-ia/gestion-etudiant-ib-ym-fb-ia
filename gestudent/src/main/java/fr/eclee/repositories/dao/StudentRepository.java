package fr.eclee.repositories.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.eclee.beans.Student;

public interface StudentRepository extends JpaRepository<Student, Integer>{

}
