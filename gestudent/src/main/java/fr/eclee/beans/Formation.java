package fr.eclee.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Formation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idFormation;

	@NonNull
	private String name;

	@NonNull
	private String manager;
	
	
	@OneToMany(mappedBy = "formation" , cascade = CascadeType.ALL)
    private List<Module> modules;
	
	@OneToMany(mappedBy = "formation" , cascade = CascadeType.ALL)
	private List<Classroom> classroom;
	
	@OneToMany(mappedBy = "formation" , cascade = CascadeType.ALL)
	private List<Student> student;
	
	
}
