package fr.eclee.beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Mark {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idMark;

	@NonNull
	private Float value;
	
	@ManyToOne  //(cascade = {CascadeType.ALL})
	@JoinColumn(name = "student_fk")
	private Student student;
	

	@ManyToOne  //(cascade = {CascadeType.ALL})
	@JoinColumn(name = "module_fk")
	private Module module;
}
