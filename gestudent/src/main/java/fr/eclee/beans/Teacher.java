package fr.eclee.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Teacher {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idTeacher;

	@NonNull
	private String name;

	@NonNull
	private String surname;

	@NonNull
	private LocalDate dateBirth;
	
	@NonNull
	private String phone;
	
	@NonNull
	private String email;
	
	@NonNull
	private String gender; /* 1 = MAN AND 2 = WOMEN */

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_address")
	private Address address;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_account")
	private Account account;
	
	@ManyToMany
	private List<Classroom> classroom;
	
	/*
	
	@OneToMany(mappedBy = "listTeacher" , cascade = CascadeType.ALL)
	private List<Classroom> classByTeacher;
	
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "listClassroom_fk")
	private Classroom listClassroom;
	
	*/
	
}
