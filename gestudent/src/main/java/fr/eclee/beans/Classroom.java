package fr.eclee.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Classroom {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idClass;

	@NonNull
	private String name;

	@NonNull
	private Integer number;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "form_fk")
	private Formation formation;
	
	
	
	@OneToMany(mappedBy = "classroom" , cascade = CascadeType.ALL)
	private List<Student> student;
	
	
	
	@ManyToMany
	private List<Teacher> teacher;
	
	
	/*
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "listTeach_fk")
	private Teacher listTeacher;
	
	@OneToMany(mappedBy = "listClassroom" , cascade = CascadeType.ALL)
	private List<Teacher> teacherByClassroom;
	
	*/
	
}
