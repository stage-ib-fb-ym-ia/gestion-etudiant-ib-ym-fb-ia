package fr.eclee.beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Appreciation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAppreciation;

	@NonNull
	private String description;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "student_fk")
	private Student student;
	
}
