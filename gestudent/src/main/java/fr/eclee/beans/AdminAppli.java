package fr.eclee.beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity

public class AdminAppli {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAdmin;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_account")
	private Account account;

}
