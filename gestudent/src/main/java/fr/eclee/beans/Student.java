package fr.eclee.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idStudent;

	@NonNull
	private String name;

	@NonNull
	private String surname;

	@NonNull
	private LocalDate dateBirth;
	
	@NonNull
	private String placeBirth;
	
	@NonNull
	private String phone;
	
	@NonNull
	private String email;
	
	@NonNull
	private String gender;
	
	@NonNull
	private LocalDate registrationDate;
	
	@NonNull
	private String lastCertification;
	
	@NonNull
	private Integer countryCode;
	

	@OneToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "addr_fk")
	private Address address;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "classr_fk")
	private Classroom classroom;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "form_fk")
	private Formation formation;
	
	
	@OneToMany(mappedBy = "student" , cascade = CascadeType.ALL)
	private List<Mark> listMark;
	
	@OneToMany(mappedBy = "student" , cascade = CascadeType.ALL)
	private List<Diploma> diploma;
	
	@OneToMany(mappedBy = "student" , cascade = CascadeType.ALL)
	private List<Appreciation> appreciation;
	
	
	
	
	
	
}
