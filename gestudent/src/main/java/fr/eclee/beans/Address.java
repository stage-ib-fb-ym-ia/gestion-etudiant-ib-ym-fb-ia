package fr.eclee.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idAddress;

	@NonNull
	private String street;

	@NonNull
	private String city;
	
	@NonNull
	private String zipCode;

	@NonNull
	private String country;
	
	@OneToOne(mappedBy = "address")
    private Student student;

	@OneToOne(mappedBy = "address")
	private UserAppli userAppli;
	
	@OneToOne(mappedBy = "address")
	private Teacher teacher;
	
}
