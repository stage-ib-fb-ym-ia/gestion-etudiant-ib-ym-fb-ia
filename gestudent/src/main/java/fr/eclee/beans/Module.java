package fr.eclee.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class Module {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idModule;

	@NonNull
	private String name; //Niveau dans le nom

	@NonNull
	private Integer numberHours;

	@NonNull
	private String description;
	
	
	@OneToMany(mappedBy = "module" , cascade = CascadeType.ALL)
	private List<Mark> listMark;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "form_fk")
	private Formation formation;
	
	@ManyToOne(cascade = {CascadeType.ALL})
	@JoinColumn(name = "teacher_fk")
	private Teacher teacher;
	
	
}
