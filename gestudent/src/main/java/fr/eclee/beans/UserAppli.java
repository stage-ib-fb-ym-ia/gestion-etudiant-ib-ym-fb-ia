package fr.eclee.beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@ToString
@Entity

public class UserAppli {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idUser;

	@NonNull
	private String name;

	@NonNull
	private String surname;

	@NonNull
	private String function;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_address")
	private Address address;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_account")
	private Account account;

}
