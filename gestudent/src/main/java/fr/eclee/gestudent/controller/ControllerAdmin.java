package fr.eclee.gestudent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Account;
import fr.eclee.beans.AdminAppli;
import fr.eclee.repositories.dao.AdminRepository;

@Controller
public class ControllerAdmin {

	@Autowired
	AdminRepository adminRepo;

	@GetMapping(value = "/addAdmin")
	public String redirectAddAdminForm() {
		return "register/registerAdmin";
	}

	@PostMapping(value = "/addAdmin")
	public ModelAndView addAdmin(ModelAndView mv, @RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd) {

		Account account = new Account(login, pwd);

		AdminAppli adminAppli = new AdminAppli();

		adminAppli.setAccount(account);

		adminRepo.saveAndFlush(adminAppli);

		mv.addObject("admin", adminAppli);

		mv.setViewName("home");

		return mv;
	}
	
	@GetMapping(value = "/deleteAdmin")
	public ModelAndView deleteAdmin(ModelAndView mv, @RequestParam(value = "idAdmin") Integer idAdmin) {

		adminRepo.deleteById(idAdmin);

		mv.setViewName("home");

		return mv;
	}

}
