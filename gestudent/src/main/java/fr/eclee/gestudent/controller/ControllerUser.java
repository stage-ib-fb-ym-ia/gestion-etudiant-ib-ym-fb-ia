package fr.eclee.gestudent.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Account;
import fr.eclee.beans.Address;
import fr.eclee.beans.AdminAppli;
import fr.eclee.beans.UserAppli;
import fr.eclee.gestudent.metier.IServiceUser;
import fr.eclee.repositories.dao.AccountRepository;
import fr.eclee.repositories.dao.AdminRepository;
import fr.eclee.repositories.dao.UserRepository;

@Controller
public class ControllerUser {

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	AccountRepository accountRepo;
	
	@Autowired
	AdminRepository adminRepo;
	
	@Autowired
	IServiceUser servUser;

	@GetMapping(value = "/addUser")
	public ModelAndView redirectAddUserForm(ModelAndView mv) {
		
		mv.setViewName("dashboardAdmin/users/addUser");
		
		return mv;
	
	}

	/**
	 * Function to add a manager
	 * 
	 * @param mv
	 * @param login
	 * @param pwd
	 * @param name
	 * @param surname
	 * @param function
	 * @param street
	 * @param city
	 * @param zipCode
	 * @param country
	 * @param idAccount
	 * @return
	 */
	@PostMapping(value = "/addUser")
	public ModelAndView addUser(ModelAndView mv,
			
			@RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "surname") String surname,
			@RequestParam(value = "function") String function,
			@RequestParam(value = "street") String street,
			@RequestParam(value = "city") String city,
			@RequestParam(value = "zipCode") String zipCode,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "idAccount") Integer idAccount) {

		Account account = new Account(login, pwd);

		Address address = new Address(street, city, zipCode, country);

		UserAppli user = new UserAppli(name, surname, function);

		user.setAccount(account);
		user.setAddress(address);
		
		servUser.addUser(user);
		
		ArrayList<UserAppli> listUser = servUser.listUser();
		
		mv.addObject("listUser", listUser);

		mv.setViewName("dashboardAdmin/users/listuser");
		
		return mv;
	}
	
	/*@PostMapping(value = "/addUser")
	public ModelAndView addUser(ModelAndView mv,
			@RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "surname") String surname,
			@RequestParam(value = "function") String function,
			@RequestParam(value = "street") String street,
			@RequestParam(value = "city") String city,
			@RequestParam(value = "zipCode") String zipCode,
			@RequestParam(value = "country") String country,
			@RequestParam(value = "idAccount") Integer idAccount) {

		Account account = new Account(login, pwd);

		Address address = new Address(street, city, zipCode, country);

		UserAppli userAppli = new UserAppli(name, surname, function);

		userAppli.setAccount(account);
		userAppli.setAddress(address);
		
		
		Account acc = accountRepo.findById(idAccount).get();
		mv.addObject("account", acc);
		
		
		userRepo.saveAndFlush(userAppli);

		mv.addObject("user", userAppli);
		
		ArrayList<UserAppli> listUser = (ArrayList<UserAppli>) userRepo.findAll();
		
		mv.addObject("listUser", listUser);

		mv.setViewName("dashboardAdmin/users/listuser");
		
		

		return mv;
	}*/

	@GetMapping(value = "/editUser")
	public ModelAndView updateUser(ModelAndView mv,
			@RequestParam(value = "idUser") Integer idUser,
			@RequestParam(value = "idAccount") Integer idAccount) {

		UserAppli userAppli = new UserAppli();

		userAppli = userRepo.findById(idUser).get();
		
		Account acc = accountRepo.findById(idAccount).get();
		mv.addObject("account", acc);

		mv.addObject("user", userAppli);

		mv.setViewName("dashboardAdmin/users/editUser");

		return mv;
	}

	@PostMapping(value = "/editUser")
	public ModelAndView updateUser(ModelAndView mv,
			/** ACCOUNT ADMIN */ 
			@RequestParam(value = "idAccount") Integer idAccount,
			
			/** USER  */
			@RequestParam(value = "idUser") Integer idUser,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "surname") String surname,
			@RequestParam(value = "function") String function,
			/** ACCOUNT  */
			@RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd,
			/** ADDRESS  */
			@RequestParam(value = "street") String street,
			@RequestParam(value = "city") String city,
			@RequestParam(value = "zipCode") String zipCode,
			@RequestParam(value = "country") String country) {
		
		UserAppli userAppli = userRepo.findById(idUser).get();

		
		userAppli.getAccount().setLogin(login);
		userAppli.getAccount().setPassword(pwd);

		userAppli.getAddress().setStreet(street);
		userAppli.getAddress().setCity(city);
		userAppli.getAddress().setZipCode(zipCode);
		userAppli.getAddress().setCountry(country);
		
		userAppli.setName(name);
		userAppli.setSurname(surname);
		userAppli.setFunction(function);

		userRepo.saveAndFlush(userAppli);
		
		Account acc = accountRepo.findById(idAccount).get();
		mv.addObject("account", acc);
		
		mv.addObject("user", userAppli);
		
		ArrayList<UserAppli> listUser = (ArrayList<UserAppli>) userRepo.findAll();
		
		mv.addObject("listUser", listUser);
		
		ArrayList<AdminAppli> listAdmin = (ArrayList<AdminAppli>) adminRepo.findAll();
        for (AdminAppli admin : listAdmin) {
            if (acc.getIdAccount() == admin.getAccount().getIdAccount()) {
                          mv.setViewName("dashboardAdmin/users/listuser");
                        }
        }


		return mv;
	}

	@GetMapping(value = "/deleteUser")
	public ModelAndView deleteUser(ModelAndView mv, 
			@RequestParam(value = "idUser") Integer idUser,
			@RequestParam(value = "idAccount") Integer idAccount) {
		
		Account acc = accountRepo.findById(idAccount).get();
		
		userRepo.deleteById(idUser);
		
		mv.addObject("account", acc);
		mv.setViewName("dashboardAdmin/users/listuser");

		return mv;
	}

	@GetMapping(value = "/listUser")
	public ModelAndView listUser(ModelAndView mv, 
			@RequestParam(value = "idAccount") Integer idAccount) {
		
		Account acc = accountRepo.findById(idAccount).get();
		
		ArrayList<UserAppli> listUser = (ArrayList<UserAppli>) userRepo.findAll();
		
		mv.addObject("account", acc);
		mv.addObject("listUser", listUser);

		mv.setViewName("dashboardAdmin/users/listuser");

		return mv;
	}
	
	@GetMapping(value = "/profilUser")
	public ModelAndView profilUserAdmin(ModelAndView mv, 
			@RequestParam(value = "idAccount") Integer idAccount,
			@RequestParam(value = "idUser") Integer idUser) {
		
		Account acc = accountRepo.findById(idAccount).get();
		
		UserAppli user = userRepo.findById(idUser).get();
		
		mv.addObject("account", acc);
		mv.addObject("user", user);

		mv.setViewName("dashboardAdmin/users/infoUser");

		return mv;
	}
	
	
}
