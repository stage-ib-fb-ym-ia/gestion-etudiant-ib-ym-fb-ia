package fr.eclee.gestudent.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fr.eclee.beans.Account;
import fr.eclee.beans.AdminAppli;
import fr.eclee.beans.Classroom;
import fr.eclee.beans.Formation;
import fr.eclee.beans.Module;
import fr.eclee.beans.Student;
import fr.eclee.beans.Teacher;
import fr.eclee.beans.UserAppli;
import fr.eclee.repositories.dao.AccountRepository;
import fr.eclee.repositories.dao.AdminRepository;
import fr.eclee.repositories.dao.ClassRepository;
import fr.eclee.repositories.dao.FormationRepository;
import fr.eclee.repositories.dao.ModuleRepository;
import fr.eclee.repositories.dao.StudentRepository;
import fr.eclee.repositories.dao.TeacherRepository;
import fr.eclee.repositories.dao.UserRepository;

@Controller
@SessionAttributes("admin")

public class AuthentificationAdmin {

	@Autowired
	AdminRepository adminRepo;
	
	@Autowired
	AccountRepository ar;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	StudentRepository studentRepo;
	
	@Autowired
	TeacherRepository teacherRepo;
	
	@Autowired
	ModuleRepository moduleRepo;
	
	@Autowired
	ClassRepository classRepo;
	
	@Autowired
	FormationRepository formRepo;
	

	@PostMapping(value = "/singinAdmin")
	public String singInAdmin(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd) {

		ArrayList<AdminAppli> listAdmin = (ArrayList<AdminAppli>) adminRepo.findAll();
		
		/** NUMBER OF USER*/
		ArrayList<UserAppli> listUser = (ArrayList<UserAppli>) userRepo.findAll();
		int userNumber = 0;
		for (UserAppli userAppli : listUser) {
			userNumber+=1;
		}
		model.addAttribute("userNumber", userNumber);
		/** ---------------- */
		
		/** NUMBER OF STUDENT*/
		ArrayList<Student> listStudent = (ArrayList<Student>) studentRepo.findAll();
		int studentNumber = 0;
		for (Student student : listStudent) {
			studentNumber+=1;
		}
		model.addAttribute("studentNumber", studentNumber);
		/** ---------------- */
		
		/** NUMBER OF TEACHER*/
		ArrayList<Teacher> listTeacher = (ArrayList<Teacher>) teacherRepo.findAll();
		int teacherNumber = 0;
		for (Teacher teacher : listTeacher) {
			teacherNumber+=1;
		}
		model.addAttribute("teacherNumber", teacherNumber);
		/** ---------------- */
		
		/** NUMBER OF CLASSROOM*/
		ArrayList<Classroom> listClassroom = (ArrayList<Classroom>) classRepo.findAll();
		int classNumber = 0;
		for (Classroom classroom: listClassroom) {
			classNumber+=1;
		}
		model.addAttribute("classNumber", classNumber);
		/** ---------------- */
		
		/** NUMBER OF CLASSROOM*/
		ArrayList<Formation> listFormation = (ArrayList<Formation>) formRepo.findAll();
		int formationNumber = 0;
		for (Formation formation: listFormation) {
			formationNumber+=1;
		}
		model.addAttribute("formationNumber", formationNumber);
		/** ---------------- */
		
		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();
		int moduleNumber = 0;
		for (Module module: listModule) {
			moduleNumber+=1;
		}
		model.addAttribute("moduleNumber", moduleNumber);
		
		
		
		

		if (listAdmin != null) {
			for (AdminAppli admin : listAdmin) {
				if (admin.getAccount().getLogin().equals(login)
						&& admin.getAccount().getPassword().equals(pwd)) {
					model.addAttribute("admin", admin);
					
					Account acc = ar.findById(admin.getAccount().getIdAccount()).get();
					if (acc.getAdminAppli() != null) {
						
						System.out.println("Succes : " + acc.getAdminAppli().getIdAdmin());
						model.addAttribute("account", acc);
					}
					return "dashboardAdmin/dashboardAdmin";
				}
			}
		}

		return "home";

	}

	@GetMapping(value = "/singoutAdmin")
	public String singOutAdmin(HttpSession session, SessionStatus status) {

		session.removeAttribute("admin");
		status.setComplete();

		return "redirect:/";
	}
}
