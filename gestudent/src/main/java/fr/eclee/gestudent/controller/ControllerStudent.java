package fr.eclee.gestudent.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Address;
import fr.eclee.beans.Appreciation;
import fr.eclee.beans.Diploma;
import fr.eclee.beans.Formation;
import fr.eclee.beans.Mark;
import fr.eclee.beans.Student;
import fr.eclee.repositories.dao.StudentRepository;

@Controller
public class ControllerStudent {
	
	@Autowired
	StudentRepository sr;
	
	
	
	/**
	 * Fonction qui redirige vers l'ajout d'un student
	 * @param mv
	 * @return
	 */
	@GetMapping(value = "/addStudent")
	public ModelAndView redirectAddStudent(ModelAndView mv) {
		
		mv.setViewName("addStudent");
		return mv;
	}
	
	@PostMapping(value = "/addStudent")
	public ModelAndView addStudent(ModelAndView mv, 
			
			/*INFO STUDENT*/
			@RequestParam(value = "name") String name,
			@RequestParam(value = "name") String surname,
			@RequestParam(value = "name") String dateBirth,
			@RequestParam(value = "name") String placeBirth,
			@RequestParam(value = "name") String phone,
			@RequestParam(value = "name") String email,
			@RequestParam(value = "name") String gender,
			@RequestParam(value = "name") String registrationDate,
			@RequestParam(value = "name") String lastCertification,
			@RequestParam(value = "name") Integer countryCode,
			
			/* INFO ADRESSE    */
			@RequestParam(value = "number") String street,
			@RequestParam(value = "name") String city,
			@RequestParam(value = "name") String zipCode,
			@RequestParam(value = "name") String country
			) {

		
		LocalDate dateBirthlocal = LocalDate.parse(dateBirth);
		LocalDate dateRegistration = LocalDate.parse(registrationDate);
		
		Formation formation = new Formation();
		
		ArrayList<Mark> listMark = new ArrayList<Mark>();
		ArrayList<Diploma> listDiploma = new ArrayList<Diploma>();
		ArrayList<Appreciation> listAppreciation= new ArrayList<Appreciation>();
		
		Student student = new Student(name, surname, dateBirthlocal, placeBirth, phone, email, gender, dateRegistration, lastCertification, countryCode);
		Address address = new Address(street, city, zipCode, country);
	
		
		student.setAddress(address);
		student.setAppreciation(listAppreciation);
		student.setDiploma(listDiploma);
		student.setFormation(formation);
		student.setListMark(listMark);

		sr.saveAndFlush(student);

		
		ArrayList<Student> listStudent = (ArrayList<Student>) sr.findAll();
		mv.addObject("listStudent", listStudent);
		
		mv.setViewName("listClass");

		return mv;
	}
	
	/**
	 * MODIFIER LE STUDENT
	 * @param mv
	 * @param idStudent
	 * @param name
	 * @param surname
	 * @param dateBirth
	 * @param placeBirth
	 * @param phone
	 * @param email
	 * @param gender
	 * @param registrationDate
	 * @param lastCertification
	 * @param countryCode
	 * @param street
	 * @param city
	 * @param zipCode
	 * @param country
	 * @return
	 */
	@PostMapping(value = "/editStudent")
	public ModelAndView editStudent(ModelAndView mv, 
			
			/*INFO STUDENT*/
			@RequestParam(value = "idStudent") Integer idStudent,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "name") String surname,
			@RequestParam(value = "name") String dateBirth,
			@RequestParam(value = "name") String placeBirth,
			@RequestParam(value = "name") String phone,
			@RequestParam(value = "name") String email,
			@RequestParam(value = "name") String gender,
			@RequestParam(value = "name") String registrationDate,
			@RequestParam(value = "name") String lastCertification,
			@RequestParam(value = "name") Integer countryCode,
			
			/* INFO ADRESSE    */
			@RequestParam(value = "number") String street,
			@RequestParam(value = "name") String city,
			@RequestParam(value = "name") String zipCode,
			@RequestParam(value = "name") String country
			) {

		
		Student student = sr.findById(idStudent).get();
		
		LocalDate dateBirthlocal = LocalDate.parse(dateBirth);
		LocalDate dateRegistration = LocalDate.parse(registrationDate);
		
		/** STUDENT */
		student.setName(name);
		student.setSurname(surname);
		student.setDateBirth(dateBirthlocal);
		student.setPlaceBirth(placeBirth);
		student.setPhone(phone);
		student.setEmail(email);
		student.setGender(gender);
		student.setRegistrationDate(dateRegistration);
		student.setLastCertification(lastCertification);
		student.setCountryCode(countryCode);
		
		/** ADDRESS */
		student.getAddress().setStreet(street);
		student.getAddress().setCity(city);
		student.getAddress().setZipCode(zipCode);
		student.getAddress().setCountry(country);
		
		
		

		sr.saveAndFlush(student);

		
		ArrayList<Student> listStudent = (ArrayList<Student>) sr.findAll();
		mv.addObject("listStudent", listStudent);
		
		
		
		mv.setViewName("listClass");

		return mv;
	}
	
	/**
	 * SUPPRESSION D'UN STUDENT
	 * @param mv
	 * @param idStudent
	 * @return
	 */
	@GetMapping(value = "/deleteStudent")
	public ModelAndView deleteStudent(ModelAndView mv, @RequestParam(value = "idStudent") Integer idStudent) {
		
		sr.deleteById(idStudent);
		
		ArrayList<Student> listStudent = (ArrayList<Student>) sr.findAll();
		mv.addObject("listStudent", listStudent);
		
		mv.setViewName("listClass");
		return mv;
	}

}
