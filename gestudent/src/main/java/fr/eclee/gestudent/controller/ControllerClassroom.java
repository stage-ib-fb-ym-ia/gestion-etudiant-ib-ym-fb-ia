package fr.eclee.gestudent.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Account;
import fr.eclee.beans.Address;
import fr.eclee.beans.Classroom;
import fr.eclee.beans.Formation;
import fr.eclee.beans.Module;
import fr.eclee.beans.Student;
import fr.eclee.beans.Teacher;
import fr.eclee.repositories.dao.ClassRepository;

@Controller
public class ControllerClassroom {
	
	@Autowired
	ClassRepository cr;
	
	/**
	 * Fonction qui permet d'etre rediriger sur l'ajout d'une classe
	 * @param mv
	 * @return
	 */
	@GetMapping(value = "/addClassroom")
	public ModelAndView redirectAddClassroom(ModelAndView mv) {
		
		mv.setViewName("addClass");
		return mv;
	}
	
	/**
	 * Fonction qui permet d'inscrire une classe
	 * @param mv
	 * @param name
	 * @param number
	 * @return
	 */
	@PostMapping(value = "/addClassroom")
	public ModelAndView addTeacher(ModelAndView mv, @RequestParam(value = "name") String name,
			@RequestParam(value = "number") Integer number) {

		
		Formation formation = new Formation();
		
		ArrayList<Student> listStudent = new ArrayList<Student>();
		ArrayList<Teacher> listTeacher = new ArrayList<Teacher>();
		
		Classroom classroom = new Classroom(name, number);
		
		classroom.setTeacher(listTeacher);
		classroom.setStudent(listStudent);
		classroom.setFormation(formation);

		cr.saveAndFlush(classroom);

		mv.addObject("classroom", classroom);
		
		ArrayList<Classroom> listClass = (ArrayList<Classroom>) cr.findAll();
		mv.addObject("listClass", listClass);
		
		mv.setViewName("listClass");

		return mv;
	}
	
	/**
	 * Fonction qui permet d'�tre redirg� vers la list des classes existante
	 * @param mv
	 * @return
	 */
	@GetMapping(value = "/listClassroom")
	public ModelAndView redirectListClassroom(ModelAndView mv) {
		
		
		ArrayList<Classroom> listClass = (ArrayList<Classroom>) cr.findAll();
		
		mv.addObject("listClass", listClass);
		mv.setViewName("listClass");
		return mv;
	}
	
	/**
	 * Fonction qui permet de modifier une classe - redirection vers la liste des classes
	 * @param mv
	 * @param idClassroom
	 * @param name
	 * @param number
	 * @return
	 */
	@PostMapping(value = "/editClassroom")
	public ModelAndView editClassroom(ModelAndView mv, @RequestParam(value = "idClassroom") Integer idClassroom, 
			@RequestParam(value = "name") String name,
			@RequestParam(value = "number") Integer number) {

		
		
		Classroom classroom = cr.findById(idClassroom).get();
		classroom.setName(name);
		classroom.setNumber(number);
		
		cr.saveAndFlush(classroom);

		mv.addObject("classroom", classroom);
		
		ArrayList<Classroom> listClass = (ArrayList<Classroom>) cr.findAll();
		mv.addObject("listClass", listClass);
		
		mv.setViewName("listClass");

		return mv;
	}
	
	/**
	 * FOnction de redirection vers la modification d'une classe
	 * @param mv
	 * @param idClassroom
	 * @return
	 */
	@GetMapping(value = "/editClassroom")
	public ModelAndView redirectEditClassroom(ModelAndView mv, @RequestParam(value = "id") Integer idClassroom) {
		
		
		Classroom classroom = cr.findById(idClassroom).get();
		
		mv.addObject("classroom", classroom);
		mv.setViewName("editClassroom");
		return mv;
	}
	
	/**
	 * fonction de suppression d'une classe
	 * @param mv
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/deleteClassroom")
	public ModelAndView deleteClassroom(ModelAndView mv, @RequestParam(value = "id") Integer id) {
		
		cr.deleteById(id);
		
		ArrayList<Classroom> listClass = (ArrayList<Classroom>) cr.findAll();
		mv.addObject("listClass", listClass);
		mv.setViewName("listClass");
		return mv;
	}

}
