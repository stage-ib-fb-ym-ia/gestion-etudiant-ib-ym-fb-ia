package fr.eclee.gestudent.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Formation;
import fr.eclee.beans.Mark;
import fr.eclee.repositories.dao.FormationRepository;

@Controller
public class ControllerFormation {
	
	@Autowired
	FormationRepository formationRepo;
	
	
	
	
	@GetMapping(value = "/addFormation")
	public String redirectAddFormationForm() {
		return "formation/addFormation";
	}
	
	
	
	@PostMapping(value = "/addFormation")
	public ModelAndView addFormation(ModelAndView mv,
			@RequestParam(value = "libelle") String libelle,
			@RequestParam(value = "manager") String manager ) {

		Formation formation = new Formation(libelle, manager);

		formationRepo.saveAndFlush(formation);

		//CHANGER// a changer
		mv.addObject("formation", formation);
		mv.setViewName("module/addModule");
		return mv;
	}
	
	
	
	@GetMapping(value = "/listFormation")
	public ModelAndView listFormation(ModelAndView mv) {

		ArrayList<Formation> listFormation = (ArrayList<Formation>) formationRepo.findAll();
		
		// a changer
		mv.addObject("listFormation", listFormation);
		mv.setViewName("formation/listFormation");
		return mv;
	}
	
	
	@GetMapping(value = "/editFormation")
	public ModelAndView modifierFormationRedirect(ModelAndView mv, @RequestParam(value = "id") Integer id) {
		
		Formation formation = formationRepo.findById(id).get();
		
		mv.addObject("formation", formation);
		mv.setViewName("formation/editFormation");
		return mv;
	}
	
	
	
	@PostMapping(value = "/editFormation")
	public ModelAndView editFormation(ModelAndView mv,
			@RequestParam(value = "idFormation") Integer idFormation,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "manager") String manager
			){


		Formation formation = formationRepo.findById(idFormation).get();
		
		formation.setName(name);
		formation.setManager(manager);
		
		formationRepo.saveAndFlush(formation);
		
		ArrayList<Formation> listFormation = (ArrayList<Formation>) formationRepo.findAll();

		// a changer
		mv.addObject("listFormation", listFormation);
		mv.setViewName("formation/listFormation");

		return mv;
	}
	
	
	@GetMapping(value = "/deleteFormation")
	public ModelAndView deleteFormation(ModelAndView mv, @RequestParam(value = "id") Integer id) {
		
		formationRepo.deleteById(id);
		
		ArrayList<Formation> listFormation = (ArrayList<Formation>) formationRepo.findAll();

		// a changer
		mv.addObject("listFormation", listFormation);
		mv.setViewName("formation/listFormation");
		return mv;
	}
	
	
	
	
	
}
