package fr.eclee.gestudent.controller;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Account;
import fr.eclee.beans.Address;
import fr.eclee.beans.Teacher;
import fr.eclee.beans.UserAppli;
import fr.eclee.repositories.dao.AccountRepository;
import fr.eclee.repositories.dao.TeacherRepository;

@Controller

public class ControllerTeacher {

	@Autowired
	TeacherRepository teacherRepo;
	
	@Autowired
	AccountRepository accountRepo;

	@GetMapping(value = "/addTeacher")
	public String redirectAddTeacherForm() {
		return "teacher/addTeacher";
	}

	@PostMapping(value = "/addTeacher")
	public ModelAndView addTeacher(ModelAndView mv, 
			
			/* INFO ACCOUNT */
			@RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd, 
			
			/* INFO TEACHER */
			@RequestParam(value = "name") String name,
			@RequestParam(value = "surname") String surname,
			@RequestParam(value = "date") String date,
			@RequestParam(value = "phone") String phone,
			@RequestParam(value = "email") String email,
			@RequestParam(value = "gender") String gender,
			
			/* INFO ADDRESS */
			@RequestParam(value = "street") String street,
			@RequestParam(value = "city") String city,
			@RequestParam(value = "zipCode") String zipCode,
			@RequestParam(value = "country") String country) {

		String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());
		
		Account account = new Account(login, pwdHash);

		Address address = new Address(street, city, zipCode, country);
		
		LocalDate dateBirth = LocalDate.parse(date);
		
		Teacher teacher = new Teacher(name, surname, dateBirth, phone, email, gender);

		teacher.setAccount(account);
		teacher.setAddress(address);

		teacherRepo.saveAndFlush(teacher);

		mv.addObject("teacher", teacher);

		mv.setViewName("home");

		return mv;
	}

	@GetMapping(value = "/updateTeacher")
	public ModelAndView updateTeacher(ModelAndView mv, @RequestParam(value = "idTeacher") Integer idTeacher) {

		Teacher teacher = new Teacher();

		teacher = teacherRepo.findById(idTeacher).get();

		mv.addObject("teacher", teacher);

		mv.setViewName("teacher/updateTeacher");

		return mv;
	}

	@PostMapping(value = "/updateTeacher")
	public ModelAndView updateTeacher(ModelAndView mv, 
			
			/* INFO ID */
			@RequestParam(value = "idTeacher") Integer idTeacher,
			
			/* INFO ACCOUNT */
			@RequestParam(value = "login") String login, 
			@RequestParam(value = "pwd") String pwd,
			
			/* INFO TEACHER */
			@RequestParam(value = "name") String name, 
			@RequestParam(value = "surname") String surname,
			@RequestParam(value = "date") String date, 
			@RequestParam(value = "phone") String phone,
			@RequestParam(value = "email") String email, 
			@RequestParam(value = "gender") String gender,
			
			/* INFO ADDRESS */
			@RequestParam(value = "street") String street, 
			@RequestParam(value = "city") String city,
			@RequestParam(value = "zipCode") String zipCode, 
			@RequestParam(value = "country") String country) {

		String pwdHash = BCrypt.hashpw(pwd, BCrypt.gensalt());

		LocalDate dateBirth = LocalDate.parse(date);
		
		Teacher teacher = teacherRepo.findById(idTeacher).get();

		teacher.setName(name);
		teacher.setSurname(surname);
		teacher.setDateBirth(dateBirth);
		teacher.setPhone(phone);
		teacher.setEmail(email);
		teacher.setGender(gender);
		
		teacher.getAddress().setStreet(street);
		teacher.getAddress().setCity(city);
		teacher.getAddress().setZipCode(zipCode);
		teacher.getAddress().setCountry(country);
		
		teacher.getAccount().setLogin(login);
		teacher.getAccount().setPassword(pwdHash);

		teacherRepo.saveAndFlush(teacher);
		
		ArrayList<Teacher> listTeacher = (ArrayList<Teacher>) teacherRepo.findAll();

		mv.addObject("listTeacher", listTeacher);

		mv.setViewName("teacher/listTeacher");

		return mv;
	}

	@GetMapping(value = "/deleteTeacher")
	public ModelAndView deleteTeacher(ModelAndView mv, @RequestParam(value = "idTeacher") Integer idTeacher) {

		teacherRepo.deleteById(idTeacher);

		mv.setViewName("home");

		return mv;
	}

	@GetMapping(value = "/listTeacher")
	public ModelAndView listTeacher(ModelAndView mv, 
			@RequestParam(value = "idAccount") Integer idAccount) {
		
		Account account = accountRepo.findById(idAccount).get();
		
		mv.addObject("account", account);

		ArrayList<Teacher> listTeacher = (ArrayList<Teacher>) teacherRepo.findAll();

		mv.addObject("listTeacher", listTeacher);

		mv.setViewName("teacher/listTeacher");

		return mv;
	}

	
	@GetMapping(value = "/teacherListStudent")
	public ModelAndView teacherListStudent(ModelAndView mv, 
			@RequestParam(value = "id") Integer idTeacher) {
		

		Teacher teacher = teacherRepo.findById(idTeacher).get();

		mv.addObject("teacher", teacher);

		mv.setViewName("dashboardTeacher/listStudent");

		return mv;
	}
	
	@GetMapping(value = "/teacherListClassroom")
	public ModelAndView teacherListClassroom(ModelAndView mv, 
			@RequestParam(value = "id") Integer idTeacher) {
		

		Teacher teacher = teacherRepo.findById(idTeacher).get();

		mv.addObject("teacher", teacher);

		mv.setViewName("dashboardTeacher/listClassroom");

		return mv;
	}
	
	@GetMapping(value = "/teacherListModule")
	public ModelAndView teacherListModule(ModelAndView mv, 
			@RequestParam(value = "id") Integer idTeacher) {
		

		Teacher teacher = teacherRepo.findById(idTeacher).get();

		mv.addObject("teacher", teacher);

		mv.setViewName("dashboardTeacher/listModule");

		return mv;
	}
	
	@GetMapping(value = "/teacherListMark")
	public ModelAndView teacherListMark(ModelAndView mv, 
			@RequestParam(value = "id") Integer idTeacher) {
		

		Teacher teacher = teacherRepo.findById(idTeacher).get();

		mv.addObject("teacher", teacher);

		mv.setViewName("dashboardTeacher/listMark");

		return mv;
	}
	

	public boolean checkExistingMail(String email) {
		
        ArrayList<String> listEmail = new ArrayList<String>();
        
        listEmail.add("test@gmail.com");
        listEmail.add("autreTest@gmail.com");
        listEmail.add("dernierTest@gmail.com");
        
        for (String emailP : listEmail) {
            if (emailP.equals(email)) {
                return true;
            }   
        }
        return false;
    }

}
