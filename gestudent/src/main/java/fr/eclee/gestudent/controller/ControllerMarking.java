package fr.eclee.gestudent.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Formation;
import fr.eclee.beans.Mark;
import fr.eclee.beans.Module;
import fr.eclee.repositories.dao.MarkRepository;
import fr.eclee.repositories.dao.ModuleRepository;

@Controller
public class ControllerMarking {
	
	@Autowired
	MarkRepository markRepo;
	
	@Autowired
	ModuleRepository moduleRepo;

	@GetMapping(value = "/addMark")
	public String redirectAddMarkForm() {
		return "mark/addMark";
	}
	
	
	
	@PostMapping(value = "/addMark")
	public ModelAndView addMark(ModelAndView mv, @RequestParam(value = "value") Float value) {

		Mark mark = new Mark(value);

		markRepo.saveAndFlush(mark);

		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();
		
		// a changer
		mv.addObject("listModule", listModule);
		mv.addObject("mark", mark);
		mv.setViewName("mark/listModuleForMark");
		return mv;
	}
	
	
	
	@GetMapping(value = "/markModule")
	public ModelAndView markModule(ModelAndView mv, 
			@RequestParam(value = "idModule") Integer idModule,
			@RequestParam(value = "idMark") Integer idMark) {

		Module module = moduleRepo.findById(idModule).get();
		Mark mark = markRepo.findById(idMark).get();
		
		mark.setModule(module);
		markRepo.saveAndFlush(mark);
		moduleRepo.saveAndFlush(module);
		

		ArrayList<Mark> listMark = (ArrayList<Mark>) markRepo.findAll();
		
		mv.addObject("listMark", listMark);
		mv.setViewName("mark/listMark");
		return mv;
	}
	
	
	@GetMapping(value = "/editMark")
	public ModelAndView modifierMarkRedirect(ModelAndView mv, 
			@RequestParam(value = "id") Integer id) {
		
		Mark mark = markRepo.findById(id).get();
		
		mv.addObject("mark", mark);
		mv.setViewName("mark/editMark");
		return mv;
	}
	
	@PostMapping(value = "/editMark")
	public ModelAndView editMark(ModelAndView mv, 
			@RequestParam(value = "idMark") Integer idMark,
			@RequestParam(value = "value") Float value){


		Mark mark = markRepo.findById(idMark).get();
		
		mark.setValue(value);
		
		markRepo.saveAndFlush(mark);
		
		ArrayList<Mark> listMark = (ArrayList<Mark>) markRepo.findAll();

		// a changer
		mv.addObject("listMark", listMark);
		mv.setViewName("mark/listMark");

		return mv;
	}
	
	@GetMapping(value = "/deleteMark")
	public ModelAndView deleteMark(ModelAndView mv, 
			@RequestParam(value = "id") Integer id) {
		
		markRepo.deleteById(id);
		
		ArrayList<Mark> listMark = (ArrayList<Mark>) markRepo.findAll();

		// a changer
		mv.addObject("listMark", listMark);
		mv.setViewName("mark/listMark");
		return mv;
	}
	
}
