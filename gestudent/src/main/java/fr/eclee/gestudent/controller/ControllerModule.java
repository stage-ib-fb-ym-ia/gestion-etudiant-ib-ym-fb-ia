package fr.eclee.gestudent.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Module;
import fr.eclee.beans.UserAppli;
import fr.eclee.repositories.dao.FormationRepository;
import fr.eclee.repositories.dao.ModuleRepository;
import fr.eclee.repositories.dao.UserRepository;

@Controller
public class ControllerModule {
	
	@Autowired
	ModuleRepository moduleRepo;
	

	@Autowired
	UserRepository userRepo;

	@Autowired
	FormationRepository formationRepo;
	

	
	
	
	//Model
	@GetMapping(value = "/addModule")
	public ModelAndView redirectAddModuleForm(ModelAndView mv,
			@RequestParam(value = "id") Integer id) {
		
		mv.setViewName("module/addModule");
		return mv;
	}
	
	
	
	
	
	
	
	
	
	@PostMapping(value = "/addModule")
	public ModelAndView addModule(ModelAndView mv,
			@RequestParam(value = "libelle") String libelle,
			@RequestParam(value = "hour") Integer hour,
			@RequestParam(value = "description") String description) {

		Module module = new Module(libelle, hour, description);
		
	
		moduleRepo.saveAndFlush(module);
		
		mv.setViewName("module/confirmAddModule");

		return mv;
	}
	
	
	/**
	 * Ajouter un module via un USER
	 * @param mv
	 * @param idUser
	 * @return
	 */
	@GetMapping(value = "/addModuleUser")
	public ModelAndView redirectAddModuleFormUser(ModelAndView mv,
			@RequestParam(value = "idUser") Integer idUser) {
		
		UserAppli user = userRepo.findById(idUser).get();
		mv.addObject(user);
		
		mv.setViewName("dashboardUser/addModuleUser");
		return mv;
	}
	
	
	@PostMapping(value = "/addModuleUser")
	public ModelAndView addModuleUser(ModelAndView mv,
			@RequestParam(value = "idUser") Integer idUser,
			@RequestParam(value = "libelle") String libelle,
			@RequestParam(value = "hour") Integer hour,
			@RequestParam(value = "description") String description) {

		Module module = new Module(libelle, hour, description);
	
		UserAppli user = userRepo.findById(idUser).get();
		
		moduleRepo.saveAndFlush(module);
		
		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();
		
		mv.addObject("user",user);
		mv.addObject("listModule",listModule);
		mv.setViewName("dashboardUser/listModule");

		return mv;
	}
	
	
	@GetMapping(value = "/more")
	public ModelAndView viewMoreModule(ModelAndView mv,
			@RequestParam(value = "idModule") Integer idModule,
			@RequestParam(value = "idUser") Integer idUser) {
		
		Module module = moduleRepo.findById(idModule).get();
		
		UserAppli user = userRepo.findById(idUser).get();

		mv.addObject("user",user);
		mv.addObject("module",module);
		
		mv.setViewName("dashboardUser/viewModuleUser");
		return mv;
	}
	
	
	@GetMapping(value = "/deleteModuleUser")
	public ModelAndView deleteModuleUser(ModelAndView mv,
			@RequestParam(value = "idModule") Integer idModule,
			@RequestParam(value = "idUser") Integer idUser) {
		
		moduleRepo.deleteById(idModule);
		
		UserAppli user = userRepo.findById(idUser).get();

		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();
		
		mv.addObject("user",user);
		mv.addObject("listModule",listModule);
		mv.setViewName("dashboardUser/listModule");

		return mv;
	}
	
	
	
	@GetMapping(value = "/listModule")
	public ModelAndView listModule(ModelAndView mv) {

		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();

		// a changer
		mv.addObject("listModule", listModule);
		mv.setViewName("module/listModule");

		return mv;
	}
	
	
	@GetMapping(value = "/listModuleUser")
	public ModelAndView listModuleUser(ModelAndView mv,
			@RequestParam(value = "idUser") Integer idUser) {

		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();
		
		UserAppli user = userRepo.findById(idUser).get();

		// a changer
		
		mv.addObject("listModule", listModule);
		mv.addObject("user", user);
		mv.setViewName("dashboardUser/listModule");

		return mv;
	}
	
	
	
	
	@GetMapping(value = "/editModule")
	public ModelAndView modifierModuleRedirect(ModelAndView mv,
			@RequestParam(value = "id") Integer id) {
		
		Module module = moduleRepo.findById(id).get();
		
		mv.addObject("module", module);
		mv.setViewName("module/editModule");
		return mv;
	}
	
	
	
	
	@PostMapping(value = "/editModule")
	public ModelAndView editModule(ModelAndView mv,
			@RequestParam(value = "idModule") Integer idModule,
			@RequestParam(value = "libelle") String libelle,
			@RequestParam(value = "hour") Integer hour,
			@RequestParam(value = "description") String description ){


		Module module = moduleRepo.findById(idModule).get();
		module.setName(libelle);
		module.setNumberHours(hour);;
		module.setDescription(description);
		
		moduleRepo.saveAndFlush(module);
		
		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();

		// a changer
		mv.addObject("listModule", listModule);
		mv.setViewName("module/listModule");

		return mv;
	}
	
	
	
	@GetMapping(value = "/deleteModule")
	public ModelAndView deleteModule(ModelAndView mv,
			@RequestParam(value = "id") Integer id) {
		
		moduleRepo.deleteById(id);
		
		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();

		// a changer
		mv.addObject("listModule", listModule);
		mv.setViewName("module/listModule");
		return mv;
	}

}
