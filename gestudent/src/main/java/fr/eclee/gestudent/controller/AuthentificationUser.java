package fr.eclee.gestudent.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import fr.eclee.beans.Account;
import fr.eclee.beans.Classroom;
import fr.eclee.beans.Formation;
import fr.eclee.beans.Module;
import fr.eclee.beans.Student;
import fr.eclee.beans.Teacher;
import fr.eclee.beans.UserAppli;
import fr.eclee.repositories.dao.AccountRepository;
import fr.eclee.repositories.dao.ClassRepository;
import fr.eclee.repositories.dao.FormationRepository;
import fr.eclee.repositories.dao.ModuleRepository;
import fr.eclee.repositories.dao.StudentRepository;
import fr.eclee.repositories.dao.TeacherRepository;
import fr.eclee.repositories.dao.UserRepository;

@Controller
@SessionAttributes("user")

public class AuthentificationUser {

	@Autowired
	UserRepository userRepo;
	
	@Autowired
	AccountRepository accountRepo;
	
	@Autowired
	TeacherRepository teacherRepo;
	
	@Autowired
	ClassRepository classRepo;
	
	@Autowired
	StudentRepository studentRepo;
	
	@Autowired
	FormationRepository formationRepo;
	
	@Autowired
	ModuleRepository moduleRepo;

	@SuppressWarnings("unused")
	@PostMapping(value = "/singinUser")
	public String singInUser(Model model, @RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd) {

		/** DECLARATION OF THE VARIABLES WE NEED **/
		
		int teacherNumber = 0;
		int classNumber = 0;
		int studentNumber = 0;
		int formationNumber = 0;
		int moduleNumber = 0;
		
		/** CALCULATION OF THE NUMBER OF ENTITIES **/
		
		/****************************************************************************************************/
		
		/** NUMBER OF TEACHER **/
		ArrayList<Teacher> listTeacher = (ArrayList<Teacher>) teacherRepo.findAll();
		
		for (Teacher teacher : listTeacher) {
			teacherNumber++;
		}
		
		model.addAttribute("teacherNumber", teacherNumber);
		
		/** NUMBER OF CLASSROOM **/
		ArrayList<Classroom> listClassroom = (ArrayList<Classroom>) classRepo.findAll();
		
		for (Classroom classroom: listClassroom) {
			classNumber++;
		}
		
		model.addAttribute("classNumber", classNumber);
		
		/** NUMBER OF STUDENT **/
		ArrayList<Student> listStudent = (ArrayList<Student>) studentRepo.findAll();
		
		for (Student student : listStudent) {
			studentNumber++;
		}
		
		model.addAttribute("studentNumber", studentNumber);
		
		/** NUMBER OF CLASSROOM **/
		ArrayList<Formation> listFormation = (ArrayList<Formation>) formationRepo.findAll();
		
		for (Formation formation: listFormation) {
			formationNumber++;
		}
		
		model.addAttribute("formationNumber", formationNumber);
		
		/** NUMBER OF MODULE **/
		ArrayList<Module> listModule = (ArrayList<Module>) moduleRepo.findAll();
		
		for (Module module: listModule) {
			moduleNumber++;
		}
		
		model.addAttribute("moduleNumber", moduleNumber);
		
		/****************************************************************************************************/
		
		ArrayList<UserAppli> listUser = (ArrayList<UserAppli>) userRepo.findAll();

		if (listUser != null) {
			for (UserAppli user : listUser) {
				if (user.getAccount().getLogin().equals(login)
						&&  user.getAccount().getPassword().equals(pwd)) {
					model.addAttribute("user", user);
					
					Account account = accountRepo.findById(user.getAccount().getIdAccount()).get();
					if (account.getUserAppli() != null) {
						
						model.addAttribute("account", account);
						model.addAttribute("user", user);

					}
					return "dashboardUser/dashboardUser";
				}
			}
		}

		return "home";

	}

	@GetMapping(value = "/singoutUser")
	public String singOutUser(HttpSession session, SessionStatus status) {

		session.removeAttribute("user");
		status.setComplete();

		return "redirect:/";
	}
}
