package fr.eclee.gestudent.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Classroom;
import fr.eclee.beans.Student;
import fr.eclee.beans.Teacher;
import fr.eclee.repositories.dao.TeacherRepository;

@Controller
@SessionAttributes("teacher")

public class AuthentificationTeacher {

	@Autowired
	TeacherRepository teacherRepo;

	@PostMapping(value = "/singinTeacher")
	public ModelAndView singInTeacher(ModelAndView model, @RequestParam(value = "login") String login,
			@RequestParam(value = "pwd") String pwd) {

		ArrayList<Teacher> listTeacher = (ArrayList<Teacher>) teacherRepo.findAll();

		if (listTeacher != null) {
			for (Teacher teacher : listTeacher) {
				if (teacher.getAccount().getLogin().equals(login)
						&& BCrypt.checkpw(pwd, teacher.getAccount().getPassword())) {
				

					model.addObject("nbStudent", 0);
					model.addObject("nbModule", 0);
					model.addObject("nbClassroom", 0);
					model.addObject("teacher", teacher);
					ArrayList<Integer> stats1 = new ArrayList<Integer>();
					
					
					int value1=0;
					int value2=5;
					int value3=10;
					int value4=25;
					int value5=1;
					int value6=2;
					int value7=3;
					int value8=4;
					stats1.add(value1);
					stats1.add(value2);
					stats1.add(value3);
					stats1.add(value4);
					stats1.add(value5);
					stats1.add(value6);
					stats1.add(value7);
					
					
			
					model.addObject("array", stats1);
					

					
					model.setViewName("dashboardTeacher/dashboard");
					return model;
				}
			}
		}else {
			model.setViewName("home");
			
			System.out.println("Aucun teacher trouv�");
			return model;
			
		}
		return null;

	}

	@GetMapping(value = "/singout")
	public String singOutTeacher(HttpSession session, SessionStatus status) {

		session.removeAttribute("teacher");
		status.setComplete();

		return "redirect:/";
	}
}
