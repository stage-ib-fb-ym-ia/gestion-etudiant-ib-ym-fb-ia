package fr.eclee.gestudent.metier;

import java.util.ArrayList;

import fr.eclee.beans.UserAppli;

public interface IServiceUser {

	/**
	 * Function to add a manager in the database
	 * 
	 * @param userAppli
	 * @return
	 */
	public UserAppli addUser(UserAppli userAppli);

	/**
	 * 
	 * @return
	 */
	public ArrayList<UserAppli> listUser();
	
}
