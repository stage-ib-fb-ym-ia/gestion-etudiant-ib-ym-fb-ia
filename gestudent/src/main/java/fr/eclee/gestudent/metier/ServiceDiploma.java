package fr.eclee.gestudent.metier;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.eclee.beans.Address;
import fr.eclee.beans.Appreciation;
import fr.eclee.beans.Diploma;
import fr.eclee.beans.Formation;
import fr.eclee.beans.Mark;
import fr.eclee.beans.Student;
import fr.eclee.repositories.dao.DiplomaRepository;

@Service
public class ServiceDiploma {
	
	@Autowired
	DiplomaRepository dr;
	
	/**
	 * 
	 * @param mv
	 * @param name
	 * @param dateObtained
	 * @param dateBirth
	 * @return
	 */
	@PostMapping(value = "/addDiploma")
	public ModelAndView addDiploma(ModelAndView mv, 
			
			/*INFO STUDENT*/
			@RequestParam(value = "name") String name,
			@RequestParam(value = "name") String dateObtained,
			@RequestParam(value = "name") Integer dateBirth
			) {

		
		LocalDate dateObtainedLocal = LocalDate.parse(dateObtained);
		
		Diploma diploma = new Diploma();
		
		Student student = new Student();
		
		diploma.setDateObtained(dateObtainedLocal);
		diploma.setBirthdate(dateBirth);
		diploma.setStudent(student);

		dr.saveAndFlush(diploma);

		
		ArrayList<Diploma> listDiploma = (ArrayList<Diploma>) dr.findAll();
		mv.addObject("listDiploma", listDiploma);
		
		mv.setViewName("listDiploma");

		return mv;
	}
	
	/**
	 * 
	 * @param mv
	 * @param idDiploma
	 * @param name
	 * @param dateObtained
	 * @param dateBirth
	 * @return
	 */
	@PostMapping(value = "/editDiploma")
	public ModelAndView editDiploma(ModelAndView mv, 
			
			/*INFO STUDENT*/
			@RequestParam(value = "idDiploma") Integer idDiploma,
			@RequestParam(value = "name") String name,
			@RequestParam(value = "name") String dateObtained,
			@RequestParam(value = "name") Integer dateBirth
			) {

		
		LocalDate dateObtainedLocal = LocalDate.parse(dateObtained);
		
		Diploma diploma = dr.findById(idDiploma).get();
		
		diploma.setDateObtained(dateObtainedLocal);
		diploma.setBirthdate(dateBirth);

		dr.saveAndFlush(diploma);

		
		ArrayList<Diploma> listDiploma = (ArrayList<Diploma>) dr.findAll();
		mv.addObject("listDiploma", listDiploma);
		
		mv.setViewName("listDiploma");

		return mv;
	}
	
	/**
	 * 
	 * @param mv
	 * @param idDiploma
	 * @return
	 */
	@GetMapping(value = "/deleteDiploma")
	public ModelAndView deleteDiploma(ModelAndView mv, 
			@RequestParam(value = "idDiploma") Integer idDiploma) {
		
		dr.deleteById(idDiploma);
		
		ArrayList<Diploma> listDiploma = (ArrayList<Diploma>) dr.findAll();
		mv.addObject("listDiploma", listDiploma);
		
		mv.setViewName("listDiploma");
		return mv;
	}

}
