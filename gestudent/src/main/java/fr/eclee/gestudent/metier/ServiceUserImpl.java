package fr.eclee.gestudent.metier;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.eclee.beans.UserAppli;
import fr.eclee.repositories.dao.UserRepository;

@Service
public abstract class ServiceUserImpl implements IServiceUser {

	@Autowired
	UserRepository userRepo;

	public UserAppli addUser(UserAppli user) {

		UserAppli userAppli = userRepo.saveAndFlush(user);

		return userAppli;

	}

	@Override
	public ArrayList<UserAppli> listUser() {
		
		return (ArrayList<UserAppli>) userRepo.findAll();

	}
}
