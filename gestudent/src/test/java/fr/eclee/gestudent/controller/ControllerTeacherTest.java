package fr.eclee.gestudent.controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ControllerTeacherTest {
	
	ControllerTeacher service = new ControllerTeacher();
	
	@Test
	public void checkExistingMail () {
		
		assertTrue(service.checkExistingMail("test@gmail.com"));
		
		assertFalse(service.checkExistingMail("testFalse"));
		
	}
}
